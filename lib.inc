section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit:
  mov rax, 60
  syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
  xor rax, rax
 .iter:
  cmp byte[rdi+rax], 0
  je .ret
  inc rax
  jmp .iter
 .ret
  ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
  push rdi
  call string_length
  pop rsi
  mov rdi, 1
  mov rdx, rax
  mov rax, 1
  syscall
  ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
  mov rdi, 0xa  

; Принимает код символа и выводит его в stdout
print_char:
  push rdi
  mov rax, 1
  mov rdi, 1
  mov rsi, rsp
  mov rdx, 1
  syscall
  pop rdi
  ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
  test rdi, rdi
  jns print_uint
  neg rdi
  push rdi
  mov rdi, 0x2d
  call print_char
  pop rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
  mov rax, rdi
  mov r9, rsp
  push 0
  mov rdi, rsp
  sub rsp, 20
  mov r10, 10
 .iter:
  xor rdx, rdx
  div r10
  add dl, '0'
  dec rdi
  mov [rdi], dl
  test rax, rax
  jnz .iter
  push r9
  call print_string
  pop rsp
  ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
  xor rax, rax
  mov al, [rdi]
  cmp al, [rsi]
  jne .unequal
  inc rdi
  inc rsi
  test al, al
  jne string_equals
  mov rax, 1
  ret
 .unequal:
  xor rax, rax
  ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
  xor rax, rax
  xor rdi, rdi
  mov rdx, 1
  push 0
  mov rsi, rsp
  syscall
  pop rax
  ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
  xor rax, rax
  push r12
  push r13
  xor r12, r12
  mov r13, rsi
 .skip:
  push rdi
  call read_char
  pop rdi
  cmp al, 0x20
  je .skip
  cmp al, 0x9
  je .skip
  cmp al, 0xa
  je .skip
  cmp al, 0x13
  je .skip
  test al, al
  jz .check
 .read:
  mov byte[rdi+r12], al
  inc r12
  push rdi
  call read_char
  pop rdi
  cmp al, 0x20
  je .check
  cmp al, 0x9
  je .check
  cmp al, 0xa
  je .check
  cmp al, 0x13
  je .check
  test al, al
  je .check
  cmp r13, r12
  je .ret
  jmp .read
 .check:
  mov byte[rdi+r12], 0
  mov rax, rdi
  mov rdx, r12
  pop r13
  pop r12
  ret
 .ret:
  xor rax, rax
  pop r13
  pop r12
  ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
  cmp byte[rdi], 0x2d
  jne parse_uint
  inc rdi
  call parse_uint
  neg rax
  inc rdx
  ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
  xor rax, rax
  mov r8, 10
  xor rcx, rcx
  xor r9, r9
 .iter:
  mov r9b, byte[rdi+rcx]
  cmp r9b, '0'
  jb .ret
  cmp r9b, '9'
  ja .ret
  mul r8
  sub r9b, '0'
  add rax, r9
  inc rcx
  jmp .iter
 .ret:
  mov rdx, rcx
  ret
 
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
  xor rax, rax
 .iter:
  cmp rax, rdx
  je .fail
  mov r9b, [rdi+rax]
  mov [rsi+rax], r9b
  test r9b, r9b
  je .ret
  inc rax
  jmp .iter
 .fail:
  xor rax, rax
 .ret:
  ret
